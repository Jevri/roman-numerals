#include <assert.h>

static int
symbol_value(char symbol)
{
  switch (symbol)
    {
    case 'I':
      return 1;
    case 'V':
      return 5;
    case 'X':
      return 10;
    case 'L':
      return 50;
    case 'C':
      return 100;
    case 'D':
      return 500;
    case 'M':
      return 1000;
     }
  return -1;
}

enum symbol_index
  {
   tens,
   fives,
   ones,
   next_set
  };

int
parse_roman_numeral(char * roman_num, int lenght)
{
  int sum = 0;
  int last_val = 0;
  int sub_mode = 0;
  int i;
  for (i = lenght - 1; i >= 0; i--)
    {
      int current_val = symbol_value(roman_num[i]);
      if (current_val == -1)
	return -1;
      if (current_val < last_val || (sub_mode && current_val == last_val))
	{
	  sum -= current_val;
	  sub_mode = 1;
	}
      else
	{
	  sum += current_val;
	  sub_mode = 0;
	}
      last_val = current_val;
    }
  return sum;
}

int
parse_roman_numeral_until(char * roman_num, int max_lenght, char ** endp)
{
  int sum1 = 0;
  int sum2 = 0;
  int last_val = 0;
  int i;
  for (i = 0; i < max_lenght; i++)
    {
      int current_val = symbol_value(roman_num[i]);
      if (current_val == -1)
	break;
      if (current_val == last_val)
        sum2 += current_val;
      else
	{
          if (current_val > last_val)
            sum1 -= sum2;
          else
            sum1 += sum2;
          sum2 = current_val;
	}
      last_val = current_val;
    }
  sum1 += sum2;
  if (endp)
    *endp = &roman_num[i];
  return sum1;
}

static int
encode_remaining(int * number, int value, unsigned char * out)
{
  int length = *number / value;
  *number %= value;
  *out = length;
  return length;
}

enum abreviaion
  {
   one,
   nine,
   four,
   five
  };

struct roman_ecoding
{
  struct
  {
    unsigned char value;
    unsigned char abreviation;
  } sets[3];
  int remaing;
};

int
encode_roman_numeral_formatting(int number, unsigned char encoding_out[7])
{
  assert(number > 0 && number <= 255999);
  static const int symbol_values[] = { 1000, 500, 100, 50, 10, 5, 1, 0 };  /* null terminated */
  int length = 0;
  const int * values = symbol_values;
  unsigned char * encoding = encoding_out;
  int value1;
  do
    {
      value1 = values[ones];
      int value5 = values[fives];
      int value4 = value5 - value1;
      int value10 = values[tens];
      int value9 = value10 - value1;

      length += encode_remaining(&number, value10, encoding);
      encoding++;

      int value1_count = number / value1;
      if (value1_count == 9)
        {
          number -= value9;
          *encoding = nine;
          length += 2;
        }
      else if (value1_count == 4)
        {
          number -= value4;
          *encoding = four;
          length += 2;
        }
      else if (value1_count >= 5)
        {
          number -= value5;
          *encoding = five;
          length += 1;
        }
      else
        *encoding = one;
      encoding++;
    }
  while (values[next_set] && (values = &values[ones]));
  return encode_remaining(&number, value1, encoding) + length;
}

void
print_roman_numeral_encoding(unsigned char encoding[7], char * buf)
{
  char * symbols = "MDCLXVI";
  do
    {
      int i;
      for (i = 0; i < *encoding; i++)
        {
          *buf = symbols[tens];
          buf++;
        }
      encoding++;
      switch (*encoding)
        {
        case one:
          break;
        case nine:
          *buf = symbols[ones];
          buf++;
          *buf = symbols[tens];
          buf++;
          break;
        case four:
          *buf = symbols[ones];
          buf++;
          *buf = symbols[fives];
          buf++;
          break;
        case five:
          *buf = symbols[fives];
          buf++;
          break;
        default:
          assert(0);
        }
      encoding++;
    }
  while (symbols[next_set] && (symbols = &symbols[ones]));
  int i;
  for (i = 0; i < *encoding; i++)
    {
      *buf = symbols[ones];
      buf++;
    }
}

#ifdef INCLUDE_ROMAN_FILE_OUTPUT

#include <stdio.h>

/* put character C N times into a file */
static void
putcn(char c, FILE * fp, int count)
{
  int i;
  for (i = 0; i < count; i++)
    putc(c, fp);
}

/* print the remaining symbols for value to a file,
   and put the remainder back into number. Return the number of characters */
static int
print_remaining(int * number, int value, char symbol, FILE * fp)
{
  int length = *number / value;
  *number %= value;
  putcn(symbol, fp, length);
  return length;
}

int
fprint_roman_numeral(int number,  FILE * fp)
{
  assert(number > 0);
  int length = 0;
  char * symbols = "MDCLXVI";
  int value1;
  do
    {
      value1 = symbol_value(symbols[ones]);
      int value5 = symbol_value(symbols[fives]);
      int value4 = value5 - value1;
      int value10 = symbol_value(symbols[tens]);
      int value9 = value10 - value1;

      length += print_remaining(&number, value10, symbols[0], fp);

      int value1_count = number / value1;
      if (value1_count == 9)
        {
          number -= value9;
          putc(symbols[ones], fp);
          putc(symbols[tens], fp);
          length += 2;
        }
      else if (value1_count == 4)
        {
          number -= value4;
          putc(symbols[ones], fp);
          putc(symbols[fives], fp);
          length += 2;
        }
      else if (value1_count >= 5)
        {
          number -= value5;
          putc(symbols[fives], fp);
          length += 1;
        }
    }
  while (symbols[next_set] && (symbols = &symbols[ones]));
  return print_remaining(&number, value1, symbols[ones], fp) + length;
}
#endif /* INCLUDE_ROMAN_FILE_OUTPUT */
