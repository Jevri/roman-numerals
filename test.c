#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include "roman-numerals.h"

enum format_status
  {
   invalid,
   strict,
   valid
  };
struct value_pair
{
  char * str;
  int value;
  enum format_status format_status;
};
int
main(int argc, char *argv[])
{
  struct value_pair numbers[] =
    { "I", 1, strict,
      "II", 2, strict,
      "IV", 4, strict,
      "V", 5, strict,
      "VI", 6, strict,
      "IIV", 3, valid,
      "VII", 7, strict,
      "IIIIIIIIII", 10, valid,
      "XIX", 19, strict,
      "IXX", 19, valid,
      "IIXX", 18, valid,
      "MCMLXXIX", 1979, strict };

  int i;
  for (i = 0; i < 12; i++)
    {
      printf("test %d == parse_roman_numeral(%s)\n", numbers[i].value, numbers[i].str);
      assert(numbers[i].value == parse_roman_numeral(numbers[i].str,
                                                     strlen(numbers[i].str)));
    }
  for (i = 0; i < 12; i++)
    {
      printf("test %d == parse_roman_numeral_untill(%s)\n",
             numbers[i].value,
             numbers[i].str);
      assert(numbers[i].value == parse_roman_numeral_untill(numbers[i].str,
                                                    strlen(numbers[i].str),
                                                    NULL));
    }
  for (i = 0; i < 12; i++)
    {
      fprint_roman_numeral(parse_roman_numeral(numbers[i].str, strlen(numbers[i].str)), stdout);
      putchar('\n');
    }
  unsigned char encoding_buffer[7];
  for (i = 0; i < 12; i++)
    {
      int number = parse_roman_numeral(numbers[i].str, strlen(numbers[i].str));
      int length = encode_roman_numeral_formatting(number, encoding_buffer);
      char * strbuf = malloc(length+1);
      print_roman_numeral_encoding(encoding_buffer, strbuf);
      strbuf[length] = '\0';
      if (numbers[i].format_status == strict)
        {
          assert(strcmp(numbers[i].str, strbuf) == 0);
        }
      else if (numbers[i].format_status == valid)
        {
          assert(strcmp(numbers[i].str, strbuf) != 0);
        }
      free(strbuf);
    }

  return 0;
}
