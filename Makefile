CFLAGS=

roman-numerals.o: roman-numerals.c
	gcc $(CFLAGS) -c roman-numerals.c

test: test.c roman-numerals.h roman-numerals.c
	gcc -o test -DINCLUDE_ROMAN_FILE_OUTPUT -g test.c roman-numerals.c

run-test: test
	./test
