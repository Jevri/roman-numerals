#ifndef _ROMAN_NUMERALS_H_
#define _ROMAN_NUMERALS_H_

/* This function will parse roman numerals in a non strict way.
   `roman_num' is a pointer to a string of characters representing a roman
   numeral. `lenght' is how many characters to read. If the string contains
   any invalid characters -1 is returned, otherwise the value of the roman
   numeral is returned */
int parse_roman_numeral(char * roman_num, int lenght);

int parse_roman_numeral_until(char * roman_num, int max_lenght, char ** endp);

/* This function will compute the roman numeral formatting for a number.
   `number' is a number between 1 and 255999 to be formatted as a roman
   numeral. `encoding_out' is a 7 byte buffer to hold the formatting. It
   returns the number of character it will take to print the formatting */
int encode_roman_numeral_formatting(int number, unsigned char encoding_out[7]);

/* This function will print the formatting output by `encode_roman_numeral_formatting'
   to a character buffer. `encoding' is the encoding output by
   `encode_roman_numeral_formatting'. `buf' is assumed to be at least the
   length returned by `encode_roman_numeral_formatting' */
void print_roman_numeral_encoding(unsigned char encoding[7], char * buf);

#ifdef INCLUDE_ROMAN_FILE_OUTPUT

#include <stdio.h>

/* This function will print formatting for a number to a file.
  `number' is a number above 0 to be formatted as a roman numeral.
  `fp' is the file the print to. It returns the number of character printed */
int fprint_roman_numeral(int number,  FILE * fp);

#endif /* INCLUDE_ROMAN_FILE_OUTPUT */

#endif /* _ROMAN_NUMERALS_H_ */
